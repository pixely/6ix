// ------------------------------------
// Constants
// ------------------------------------
export const SCHEDULE_ADD = 'SCHEDULE_ADD'
export const SCHEDULE_CHANGE = 'SCHEDULE_CHANGE'

// ------------------------------------
// Actions
// ------------------------------------
export function add(item) {
  return {
    type: SCHEDULE_ADD,
    payload: item
  }
}

export function change(item) {
  return {
    type: SCHEDULE_CHANGE,
    payload: item
  }
}

export const actions = {
  add,
  change
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SCHEDULE_ADD]: (state, action) => [...state, action.payload],
  [SCHEDULE_CHANGE]: (state, action) => state.map(item => {
    if (item.id == action.payload.id) {
      return { ...item, hour: action.payload.hour, minute: action.payload.minute }
    } else {
      return item
    }
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = []
export default function scheduleReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
